//
//  BallObject.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit


class Ball: SKSpriteNode {
    
    let type: colorType
    var isActive: Bool = true
    
    
    init () {
        let randomTypeIndex = Int(arc4random()%7)
        self.type = colorWheelOrder[randomTypeIndex]
        
        let ballTexture = SKTexture(imageNamed: "b_\(self.type)")
        
        super.init(texture: ballTexture, color: SKColor.clear, size: ballTexture.size())
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: 55)
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.categoryBitMask = physicsCategories.Ball // Физ категория для проверки контакта с другим объектом
        self.physicsBody!.collisionBitMask = physicsCategories.None // столкновение
        self.physicsBody!.contactTestBitMask = physicsCategories.Side
        
        self.setScale(0)
        
        let scaleIn = SKAction.scale(to: 1, duration: 0.2)
        let randomSizeIndex = Int(arc4random()%7)
        let sideToMoveTo = sidePositions[randomSizeIndex]
        let moveToSide = SKAction.move(to: sideToMoveTo, duration: ballMS)
        let ballSpawnSequence = SKAction.sequence([scaleIn, moveToSide])
        self.run(ballSpawnSequence)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func delete() {
        self.isActive = false
        self.removeAllActions()
        let scaleDown = SKAction.scale(by: 0, duration: 0.2)
        let deleteBall = SKAction.removeFromParent()
        let  deleteSequence = SKAction.sequence([scaleDown, deleteBall])
        self.run(deleteSequence)
        
    }
    
    func flash () {
        self.removeAllActions()
        self.isActive = false
        let fadeOut = SKAction.fadeOut(withDuration: 0.4)
        let fadeIn = SKAction.fadeIn(withDuration: 0.4)
        let flashSequence = SKAction.sequence([fadeOut, fadeIn])
        let repeatFlash = SKAction.repeat(flashSequence, count: 3)
        self.run(repeatFlash)
        
    }
    
    
    
}
