//
//  ModelAndData.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit

enum colorType {
    case Red
    case Orange
    case LBlue
    case Blue
    case Yellow
    case Purple
    case Green
}


let colorWheelOrder: [colorType] = [
    colorType.Red,
    colorType.Orange,
    colorType.Yellow,
    colorType.Green,
    colorType.LBlue,
    colorType.Blue,
    colorType.Purple
]

var sidePositions:[CGPoint] = []


enum gameState {
    case beforeGame
    case inGame
    case pauseGame
    case afterGame
}



struct physicsCategories {
    static let None: UInt32 = 0
    static let Ball: UInt32 = 0b1
    static let Side: UInt32 = 0b10
}

var highScore = UserDefaults.standard.integer(forKey: "highScoreSaved")
var highScoreSurvival = UserDefaults.standard.integer(forKey: "highScoreSurvival")


var score: Int = 0

var ballMS: TimeInterval = 4

var livesNumber = 3

var FXstatus = SKLabelNode()


