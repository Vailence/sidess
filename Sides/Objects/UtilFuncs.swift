//
//  UtilFuncs.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit

func convertDegreesToRadians (degrees: CGFloat) -> CGFloat {
    return degrees * 0.0174533
}

