//
//  SideObject.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit

class Side: SKSpriteNode {
    
    let type: colorType
    
    init (type: colorType) {
        self.type = type
        
        let sideTexture = SKTexture (imageNamed: "Logo_\(self.type)")
        
        super.init (texture: sideTexture, color: SKColor.clear, size: sideTexture.size())
        
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.categoryBitMask = physicsCategories.Side
        self.physicsBody!.collisionBitMask = physicsCategories.None
        self.physicsBody!.contactTestBitMask = physicsCategories.Ball
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
