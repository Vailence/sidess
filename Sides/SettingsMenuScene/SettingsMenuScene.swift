//  SettingsMenuScene.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 28.03.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit
import Firebase
import FirebaseUI
 
var soundFX = SKSpriteNode()
var soundXImg = SKSpriteNode()

class SettingsMenuScene: SKScene {
    let exitImg = SKSpriteNode(imageNamed: "Exit")
    var gameTableView = GameRoomTableView()
    private var label : SKLabelNode?
    
    let check = UserDefaults.standard.value(forKey: "check") as? Bool
    
    override func didMove(to view: SKView) {
        createUI()
        }
    
    override func sceneDidLoad() {
        super .sceneDidLoad()
        }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let pointITouches = touch.location (in: self)
            let tappedNode = atPoint(pointITouches)
            let tappedNodeName = tappedNode.name
    
            if tappedNodeName == "sON"  {
                FXstatus.isHidden = false
                soundFX.name = "sOFF"
                UserDefaults.standard.set(true, forKey: "check")
                soundXImg.isHidden = true
                }

            else if tappedNodeName == "sOFF"{
                FXstatus.isHidden = true
                soundFX.name = "sON"
                UserDefaults.standard.set(false, forKey: "check")
                soundXImg.isHidden = false
                }
            
            if tappedNodeName == "Leaderboard" {
                self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
                if let label = self.label {
                    label.alpha = 0.0
                    label.run(SKAction.fadeIn(withDuration: 2.0))
                    }
                // Table setup
                gameTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
                gameTableView.frame=CGRect(x:0,y:20,width:500,height:500)
                self.scene?.view?.addSubview(gameTableView)
                gameTableView.reloadData()
                exitImg.name = "ExitLeader"
                }
                
                if tappedNodeName == "ExitLeader" {
                    gameTableView.removeFromSuperview()
                    exitImg.name = "buttonBack"
                    }
            
                if tappedNodeName == "musicOnOFF"  {
                    if backMusic.isPlaying {
                        backMusic.stop()
                        }
                    else {
                        backMusic.play()
                        }
                    }
             
                if tappedNodeName == "buttonBack" {
                    let sceneToMoveTo = MainMenuSScene(size: self.size)
                    sceneToMoveTo.scaleMode = self.scaleMode
                    let sceneTransition = SKTransition.fade(withDuration: 0.8)
                    self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
                    }
                }
        }
    
    func createUI() {
        let backgroundPic = SKSpriteNode(imageNamed: "preview")
        backgroundPic.size = self.size
        backgroundPic.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        backgroundPic.zPosition = -3
        self.addChild(backgroundPic)
        
        // Лого игры
        let logoImg = SKSpriteNode(imageNamed: "Logotype")
        logoImg.size = CGSize(width: 900, height: 900)
        logoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.74)
        logoImg.zPosition = -2
        self.addChild(logoImg)
        
        // Название игры
        let textLogoImg = SKSpriteNode(imageNamed: "COLORS")
        textLogoImg.size = CGSize(width: 700, height: 150)
        textLogoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.75)
        textLogoImg.zPosition = -1
        self.addChild(textLogoImg)
        
        
        let leaderboard = SKSpriteNode(imageNamed: "Leaderboard")
        leaderboard.size = CGSize(width: 210, height: 175)
        leaderboard.position = CGPoint(x: self.size.width/3.46, y: self.size.height*0.33)
        leaderboard.zPosition = 1
        leaderboard.name = "Leaderboard"
        self.addChild(leaderboard)
        
        
        let musicLabel = SKSpriteNode(imageNamed: "Music")
        musicLabel.size = CGSize(width: 180, height: 180)
        musicLabel.position = CGPoint(x: self.size.width/1.4, y: self.size.height*0.33)
        musicLabel.zPosition = 1
        musicLabel.name = "musicOnOFF"
        self.addChild(musicLabel)
        
        // zvukEffekti
        soundFX = SKSpriteNode(imageNamed: "FxON")
        soundFX.size = CGSize(width: 130, height: 180)
        soundFX.position = CGPoint(x: self.size.width/2.1, y: self.size.height*0.33)
        soundFX.zPosition = 1
        soundFX.name = "sON"
        self.addChild(soundFX)
        
        soundXImg = SKSpriteNode(imageNamed: "Exit")
        soundXImg.size = CGSize(width: 80, height: 80)
        soundXImg.position = CGPoint(x: self.size.width/1.75, y: self.size.height*0.33)
        soundXImg.zPosition = 1
        soundXImg.name = "soundXImg"
        self.addChild(soundXImg)
        
        
        exitImg.size = CGSize(width: 100, height: 100)
        exitImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.13)
        exitImg.zPosition = 1
        exitImg.name = "buttonBack"
        self.addChild(exitImg)
        
        
        let animateList = SKAction.sequence([SKAction.fadeIn(withDuration: 1.0), SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1.0)])
        let animateList2 = SKAction.sequence([SKAction.rotate(byAngle: -360/7, duration: 100)])
        
        textLogoImg.run(SKAction.repeatForever(animateList))
        logoImg.run(SKAction.repeatForever(animateList2))
        }
}
