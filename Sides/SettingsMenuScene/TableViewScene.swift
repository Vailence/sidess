

import Foundation
import SpriteKit
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

struct postStructure {
    let name: String!
    let score: String!
}

class GameRoomTableView: UITableView,UITableViewDelegate,UITableViewDataSource {
    
    var posts = [postStructure]()
    
    var ref: DatabaseReference?
    var postData = [String]()
    var databaseHandle: DatabaseHandle?

    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
        
        ref = Database.database().reference()
        ref?.child("users").queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let name = value?["Name"] as? String
            if snapshot.hasChild(name!) {
                print("true rooms exist")
            }
            else {
                print("false rooms doesn't exist")
            }
            let score = value?["Score"] as! Int
            self.posts.insert(postStructure(name: name, score: "\(score)"), at: 0)
            })
        
//        databaseHandle = ref?.child("users").observe(.childAdded, with: { (snapshot) in
//            let post = snapshot.value as? String
//            if let actualPost = post {
//                self.postData.append(actualPost)
//                self.reloadData()
//            }
        }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return postData.count
        return posts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.textLabel?.text = self.posts[indexPath.row].name + " : " + self.posts[indexPath.row].score + " point"
//        cell.textLabel?.text = self.posts[indexPath.row].score

        if (indexPath.row % 2 == 0){
            cell.backgroundColor = UIColor.lightGray
        }
        else{
            cell.backgroundColor = UIColor.darkGray}
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.black.cgColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Leaderboard"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
}

