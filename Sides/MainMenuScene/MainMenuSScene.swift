//
//  MainMenuSScene.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 08.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit
import Firebase

class MainMenuSScene: SKScene  {
   
    override func didMove(to view: SKView) {
        createUI()
        }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        for touch: AnyObject in touches {
            let pointITouches = touch.location (in: self)
            let tappedNode = atPoint(pointITouches)
            let tappedNodeName = tappedNode.name
         
            if tappedNodeName == "playy" {
                let sceneToMoveTo = ChooseModeScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                self.view!.presentScene(sceneToMoveTo, transition: SKTransition.fade(withDuration: TimeInterval(0.7)))
                }
            
            if tappedNodeName == "Settings1" {
                let sceneToMoveTo = SettingsMenuScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                self.view!.presentScene(sceneToMoveTo, transition: SKTransition.fade(withDuration: TimeInterval(0.7)))
                }
            
            if tappedNodeName == "Credits1" {
                try! Auth.auth().signOut()
                }
            
            }
        }
    
    func createUI() {
        // Задний фон
        let backgroundPic = SKSpriteNode(imageNamed: "preview")
        backgroundPic.size = self.size
        backgroundPic.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        backgroundPic.zPosition = -3
        self.addChild(backgroundPic)
        
        // Лого игры
        let logoImg = SKSpriteNode(imageNamed: "Logotype")
        logoImg.size = CGSize(width: 900, height: 900)
        logoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.74)
        logoImg.zPosition = -2
        self.addChild(logoImg)
        
        // Название игры
        let textLogoImg = SKSpriteNode(imageNamed: "COLORS")
        textLogoImg.size = CGSize(width: 700, height: 150)
        textLogoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.75)
        textLogoImg.zPosition = -1
        self.addChild(textLogoImg)
        
        // Кнопка начать игру
        let playBtn = SKSpriteNode(imageNamed: "Play")
        playBtn.size = CGSize(width: 250, height: 250)
        playBtn.position = CGPoint(x: self.size.width/2, y: self.size.height*0.40)
        playBtn.zPosition = 1
        playBtn.name = "playy"
        self.addChild(playBtn)
        
        // Кнопка настройки
        let settingsBtn = SKSpriteNode(imageNamed: "Settings")
        settingsBtn.size = CGSize(width: 250, height: 250)
        settingsBtn.position = CGPoint(x: self.size.width/3, y: self.size.height*0.22)
        settingsBtn.zPosition = 1
        settingsBtn.name = "Settings1"
        self.addChild(settingsBtn)
        
        // Кнопка Об авторе
        let creditsBtn = SKSpriteNode(imageNamed: "Credits")
        creditsBtn.size = CGSize(width: 250, height: 250)
        creditsBtn.position = CGPoint(x: self.size.width/1.5, y: self.size.height*0.22)
        creditsBtn.zPosition = 1
        creditsBtn.name = "Credits1"
        self.addChild(creditsBtn)
        
        
        let animateList = SKAction.sequence([SKAction.fadeIn(withDuration: 1.0), SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1.0)])
        let animateList2 = SKAction.sequence([SKAction.rotate(byAngle: -360/7, duration: 100)])
        
        textLogoImg.run(SKAction.repeatForever(animateList))
        logoImg.run(SKAction.repeatForever(animateList2))
        }
}





