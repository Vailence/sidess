//
//  GameOverScene.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit
import GoogleMobileAds

class GameOverScene: SKScene {
    
    
    let ScoreLbl = SKLabelNode(fontNamed: "CaviarDreams")
   let restartBtn = SKSpriteNode(imageNamed: "Repeat")
    
    
    override func didMove(to view: SKView) {
        
        let backgroundPic = SKSpriteNode(imageNamed: "preview")
        backgroundPic.size = self.size
        backgroundPic.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        backgroundPic.zPosition = -3
        self.addChild(backgroundPic)
        
        // Лого игры
        let logoImg = SKSpriteNode(imageNamed: "Logotype")
        logoImg.size = CGSize(width: 900, height: 900)
        logoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.74)
        logoImg.zPosition = -2
        self.addChild(logoImg)
        
        
        
        ScoreLbl.position = CGPoint(x: self.size.width/2, y: self.size.height*0.75)
        ScoreLbl.fontSize = 100
        ScoreLbl.fontColor = SKColor.black
        ScoreLbl.zPosition = 1
        ScoreLbl.text = "Score: \(score)"
        self.addChild(ScoreLbl)

        let HighScoreLbl = SKLabelNode(fontNamed: "CaviarDreams")
        HighScoreLbl.position = CGPoint(x: self.size.width/2, y: self.size.height*0.70)
        HighScoreLbl.fontSize = 100
        HighScoreLbl.fontColor = SKColor.black
        HighScoreLbl.zPosition = 1
        HighScoreLbl.text = "High Score: \(highScore)"
        self.addChild(HighScoreLbl)
        
        
        // Название игры
//        let textLogoImg = SKSpriteNode(imageNamed: "COLORS")
//        textLogoImg.size = CGSize(width: 700, height: 150)
//        textLogoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.75)
//        textLogoImg.zPosition = -1
//        self.addChild(textLogoImg)
        
        // Кнопка начать игру
        let ExitBtn = SKSpriteNode(imageNamed: "Exit")
        ExitBtn.size = CGSize(width: 250, height: 250)
        ExitBtn.position = CGPoint(x: self.size.width/2, y: self.size.height*0.22)
        ExitBtn.zPosition = 1
        ExitBtn.name = "Exit"
        self.addChild(ExitBtn)
        
        // Кнопка настройки
        
        restartBtn.size = CGSize(width: 250, height: 250)
        restartBtn.position = CGPoint(x: self.size.width/2, y: self.size.height*0.4)
        restartBtn.zPosition = 1
        restartBtn.name = "AdVideo"
        self.addChild(restartBtn)
        
        
        
        
//        let background  = SKSpriteNode(imageNamed: "preview")
//        backgroundPic.size = self.size
//        backgroundPic.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
//        backgroundPic.zPosition = -3
//        self.addChild(backgroundPic)
//
//        // Лого игры
//        let logoImg = SKSpriteNode(imageNamed: "Logotype")
//        logoImg.size = CGSize(width: 900, height: 900)
//        logoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.74)
//        logoImg.zPosition = -2
//        self.addChild(logoImg)
//
//        // Название игры
//        let textLogoImg = SKSpriteNode(imageNamed: "COLORS")
//        textLogoImg.size = CGSize(width: 700, height: 150)
//        textLogoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.75)
//        textLogoImg.zPosition = -1
//        self.addChild(textLogoImg)
//
//
//        let exitBtn = SKSpriteNode(imageNamed: "Exit")
//        exitBtn.size = CGSize(width: 250, height: 250)
//        exitBtn.position = CGPoint(x: self.size.width/2, y: self.size.height*0.40)
//        exitBtn.zPosition = 1
//        exitBtn.name = "exitBtn"
//        self.addChild(exitBtn)
//
//        let restartLabel = SKSpriteNode(imageNamed: "Repeat")
//        restartLabel.size = CGSize(width: 250, height: 250)
//        restartLabel.position = CGPoint(x: self.size.width/2, y: self.size.height*0.30)
//        restartLabel.zPosition = 1
//        restartLabel.name = "restart"
//        self.addChild(exitBtn)
      
    }
    
    
    override func sceneDidLoad() {
        super .sceneDidLoad()
      
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let pointITouches = touch.location (in: self)
            let tappedNode = atPoint(pointITouches)
            let tappedNodeName = tappedNode.name
            
            if tappedNodeName == "Exit"  {
                livesNumber = 3
                let sceneToMoveTo = MainMenuSScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let sceneTransition = SKTransition.fade(withDuration: 0.5)
                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
            }
            
            if tappedNodeName == "AdVideo"  {
                if let viewController = view?.window?.rootViewController {
//                    SwiftyAd.shared.showBanner(from: viewController)
//                    SwiftyAd.shared.showBanner(from: viewController, at: .top) // Shows banner at the top
                    SwiftyAd.shared.showInterstitial(from: viewController)
                    SwiftyAd.shared.showInterstitial(from: viewController, withInterval: 4) // Shows an ad every 4th time method is called
                    SwiftyAd.shared.showRewardedVideo(from: viewController) // Should be called when pressing dedicated button
                    
                }
                restartBtn.name = "Repeat"
                }
            if tappedNodeName == "Repeat" {
                livesNumber += 1
                let sceneToMoveTo = GameScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let sceneTransition = SKTransition.fade(withDuration: 0.5)
                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
                
                restartBtn.name = "AdVideo"
            }
        }
    }
}
