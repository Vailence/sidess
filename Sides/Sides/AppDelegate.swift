//
//  AppDelegate.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GADMobileAds.configure(withApplicationID: "ca-app-pub-9621242459199204~4850631261")
        SwiftyAd.shared.setup(
            withBannerID:    "Enter your real id or leave empty if unused",
            interstitialID:  "Enter your real id or leave empty if unused",
            rewardedVideoID: "ca-app-pub-9621242459199204/1467495777"
            )
        return true
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    
}

