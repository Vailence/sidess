//
//  GameScene.swift
//  7 sides
//
//  Created by Vailence on 17.12.2017.
//  Copyright © 2017 Vailence. All rights reserved.
//

import SpriteKit
import GameplayKit


let bestLabel = SKLabelNode()
let bestScore = SKLabelNode()
let livesLabel = SKLabelNode()
let tapToStartLabel = SKLabelNode()

var pauseImg = SKSpriteNode(imageNamed: "Pause")
let topBar = SKSpriteNode()
var pausePanel = SKSpriteNode()

class GameScene: SKScene, SKPhysicsContactDelegate {
    let timerCountLabel = SKLabelNode()
    let scoreLabel = SKLabelNode()
    
    var colorWheelBase = SKShapeNode()
    let spinColorWheel = SKAction.rotate(byAngle: -convertDegreesToRadians(degrees: 360/7), duration: 0.2)
    
    var currentGameState: gameState = gameState.beforeGame
    
    //Звуки при попадании шарика
    let playCorrectSound = SKAction.playSoundFileNamed("correctSound.wav", waitForCompletion: false)
    let playInCorrectSound = SKAction.playSoundFileNamed("incorrectSound.wav", waitForCompletion: false)
    
    override func didMove(to view: SKView){
        score = 0
        ballMS = 4
        self.physicsWorld.contactDelegate = self
        createUI()
        prepColorWheel()
        }
    
    func createUI(){
        let background = SKSpriteNode(imageNamed: "preview")
        background.size = self.size
        background.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        background.zPosition = -1000
        self.addChild(background)
        
        colorWheelBase = SKShapeNode(rectOf: CGSize(width: self.size.width*0.7, height: self.size.width*0.7))
        colorWheelBase.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        colorWheelBase.fillColor = SKColor.clear
        colorWheelBase.strokeColor = SKColor.clear
        self.addChild(colorWheelBase)
        
        //кнопка "Старт" и ее параметры
        tapToStartLabel.text = "Tap to Start"
        tapToStartLabel.fontSize = 150
        tapToStartLabel.fontColor = SKColor.black
        tapToStartLabel.position = CGPoint (x: self.size.width/2, y: self.size.height/2.1)
        self.addChild(tapToStartLabel)
        
        scoreLabel.text = "\(score)"
        scoreLabel.position = CGPoint (x: self.size.width/2, y: self.size.height * 0.915)
        scoreLabel.fontColor = SKColor.white
        scoreLabel.fontSize = 160
        self.addChild(scoreLabel)
        
        // кнопка "лучший счет" и ее параметры
        bestLabel.text = "BEST"
        bestLabel.position = CGPoint (x: self.size.width/3, y: self.size.height * 0.955)
        bestLabel.fontColor = SKColor.white
        bestLabel.fontSize = 70
        self.addChild(bestLabel)
        
        bestScore.text = "\(highScore)"
        bestScore.position = CGPoint (x: self.size.width/3.01, y: self.size.height * 0.906)
        bestScore.fontColor = SKColor.white
        bestScore.fontSize = 100
        self.addChild(bestScore)
        
        // кнопка "Жизни" и ее параметры
        livesLabel.text = "Lives: \(livesNumber)"
        livesLabel.fontSize = 100
        livesLabel.fontColor = SKColor.white
        livesLabel.position = CGPoint(x: self.size.width/1.4, y: self.size.height*0.93)
        livesLabel.zPosition = 1
        self.addChild(livesLabel)
      
        pauseImg.size = CGSize(width: 100, height: 100)
        pauseImg.position = CGPoint(x: self.size.width/5.45, y: self.size.height*0.946)
        pauseImg.zPosition = 3
        pauseImg.name = "Pause"
        self.addChild(pauseImg)
        
        topBar.color = .gray
        topBar.position = CGPoint(x: self.size.width/2, y: self.size.height*0.95)
        topBar.size = CGSize(width: self.frame.size.width, height: 250)
        topBar.zPosition = -800
        addChild(topBar)
        }
    
    //функция поворота Сторон
    func prepColorWheel() {
        for i in 0...6 {
            let side = Side(type: colorWheelOrder[i])
            let basePosition = CGPoint (x: self.size.width/2, y: self.size.height*0.25)
            side.position = convert(basePosition, to: colorWheelBase)
            side.zRotation = -colorWheelBase.zRotation
            colorWheelBase.addChild(side)
            colorWheelBase.zRotation += convertDegreesToRadians(degrees: 360/7)
            }
        for side in colorWheelBase.children {
            let sidePosition = side.position
            let positionInScene = convert(sidePosition, from: colorWheelBase)
            sidePositions.append (positionInScene)
            }
            colorWheelBase.setScale(0.9)
        }
    
    // функция генерации шарика
    func spawnBall() {
        let ball = Ball()
        ball.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        self.addChild(ball)
        }
    
    //функция запуска игры
    func startTheGame () {
        spawnBall() //генерируется шарик
        currentGameState = .inGame //изменяется статус игры на "В игре"
      
        let scaleDown = SKAction.scale(to: 0, duration: 0.1)
        let deleteLabel = SKAction.removeFromParent()
        let deleteSequence = SKAction.sequence([scaleDown, deleteLabel])
        tapToStartLabel.run(deleteSequence)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let pointITouches = touch.location (in: self)
            let tappedNode = atPoint(pointITouches)
            let tappedNodeName = tappedNode.name
    
            //если статус игры "До игры", то запускаем игру
            if currentGameState == .beforeGame {
                startTheGame()
                }
                
            // если статус игры "В игре", то крутим колесо
            else if currentGameState == .inGame {
                colorWheelBase.run(spinColorWheel)
                }
            
            if tappedNodeName == "Pause" {
                if (self.isPaused == false) {
                    bestLabel.isHidden = true
                    bestScore.isHidden = true
                    scoreLabel.isHidden = true
                    livesLabel.isHidden = true
                    pauseImg.isHidden = true
                    createPausePanel()
                    }
                }
            
            if tappedNodeName == "Resume" {
                    pausePanel.removeFromParent();
                    self.scene?.isPaused = false;
                    bestLabel.isHidden = false
                    bestScore.isHidden = false
                    self.scoreLabel.isHidden = false
                    livesLabel.isHidden = false
                    pauseImg.isHidden = false
                    }
        
            
            if tappedNodeName == "Quit" {
                    let sceneToMoveTo = MainMenuSScene(size: self.size)
                    sceneToMoveTo.scaleMode = self.scaleMode
                    self.view!.presentScene(sceneToMoveTo, transition: SKTransition.fade(withDuration: TimeInterval(0.7)))
                    bestLabel.isHidden = false
                    bestScore.isHidden = false
                    scoreLabel.isHidden = false
                    livesLabel.isHidden = false
                    pauseImg.isHidden = false
                    }
            }
        }
    
    func createPausePanel() {
        self.scene?.isPaused = true;
        
        pausePanel = SKSpriteNode(imageNamed: "PausePanel");
        pausePanel.anchorPoint = CGPoint(x: 0.5, y: 0.5);
        pausePanel.size = CGSize(width: 1100, height: 1100)
        pausePanel.position = CGPoint(x: self.size.width/2, y: self.size.height/2);
        pausePanel.zPosition = 7;
        
        let resume = SKSpriteNode(imageNamed: "Play");
        let quit = SKSpriteNode(imageNamed: "Exit");
        
        resume.name = "Resume";
        resume.size = CGSize(width: 250, height: 250)
        resume.anchorPoint = CGPoint(x: 0.5, y: 0.5);
        resume.position = CGPoint(x: +200, y: 0);
        resume.zPosition = 9;
        
        quit.name = "Quit";
        quit.size = CGSize(width: 250, height: 250)
        quit.anchorPoint = CGPoint(x: 0.5, y: 0.5);
        quit.position = CGPoint(x: -200 , y: 0)
        quit.zPosition = 9;
        
        pausePanel.addChild(resume);
        pausePanel.addChild(quit);
        self.addChild(pausePanel);
        }
    
    //функция проверки соприкосновения шарика и стороны колеса
    func didBegin(_ contact: SKPhysicsContact) {
        let ball: Ball
        let side: Side
        
        if contact.bodyA.categoryBitMask == physicsCategories.Ball {
            ball = contact.bodyA.node! as! Ball
            side = contact.bodyB.node! as! Side
            }
        else{
            ball = contact.bodyB.node! as! Ball
            side = contact.bodyA.node! as! Side
            }
        
        if ball.isActive == true {
            checkMatch(ball: ball, side: side)
            }
        }
    
    //функция проверки Правильно ли попал шарик
    func checkMatch(ball: Ball, side: Side){
        //если тип шарика и тип стороны одинаковы, то запуск функции CorrectMatch
        if ball.type == side.type {
            correctMatch(ball: ball)
            }
        //если тип шарика и тип стороны разные, то запуск функции WrongMatch
        else{
            wrongMatch(ball: ball)
            loseLife(ball:ball) //Минус одна жизнь
            }
        }

    func continueMatch (ball: Ball){ //продолжение матча после потери жизни
            ball.delete() //удаляет шарик
            spawnBall()
            }
    
    func correctMatch (ball: Ball){
        ball.delete() //удаляет шарик
        score += 1 //добавляет +1 к счету
        scoreLabel.text  = "\(score)" //изменяет текст счета
        
        switch score {
        //меняет скорость шарика в зависимости от счета
            case 5: ballMS = 3.5
            case 10: ballMS = 3.0
            case 15: ballMS = 2
            case 20: ballMS = 1
            default: print("LvlUP")
            }
        spawnBall()
        
        //если "счет" выше чем "Лучший счет", то изменяется "Лучший счет"
        if score > highScore {
            bestScore.text = "\(score)"
            }
        if FXstatus.isHidden == true {
            self.run(playCorrectSound) // при правильном попадании играется звук
            }
        }
    
    func wrongMatch(ball: Ball) {
        if score > highScore {
            highScore = score
            UserDefaults.standard.set(highScore, forKey: "highScoreSaved")
            }
        ball.flash() //функция из BallObject.swift
        
        if FXstatus.isHidden == true {
            self.run(playInCorrectSound)
            } // при неправильном попадании играется звук
        }
    
    func loseLife(ball: Ball) { //функция потери жизни
        livesNumber -= 1 //удаляет одну жизнь из глобальной переменной сверху
        livesLabel.text = "Lives: \(livesNumber)" //меняет количество жизней в Label
        let scaleUP = SKAction.scale(to: 1.2, duration: 0.3)
        let scaleDown = SKAction.scale(to: 0.8, duration: 0.3)
        let scaleSequence = SKAction.sequence([scaleUP, scaleDown])
        livesLabel.run(scaleSequence) //запуск анимации
        
        if livesNumber == 0 {
            runGameOver()
            }
        else{
            continueMatch(ball: ball)
            }
        }
    
    func runGameOver() {
        // ПРОВЕРКА ПРОВЕРКА ПРОВЕРКА ПРОВЕРКА ПРОВЕРКА ПРОВЕРКА ПРОВЕРКА
        currentGameState = .afterGame //изменяет статус игры на "После игры"
        colorWheelBase.removeAllActions() //удаляет все
        //timerLabel.isHidden = false
    
        let waitToChangeScene = SKAction.wait(forDuration: 3) //ждет 3 секунды до изменения сцены
        let changeScene = SKAction.run {
            let sceneToMoveTo = GameOverScene(size: self.size) //меняет сцену
            sceneToMoveTo.scaleMode = self.scaleMode //параметры
            let sceneTransition = SKTransition.fade(withDuration: 0.5)// параметры
            self.view!.presentScene(sceneToMoveTo, transition: sceneTransition) //параметры
            }
        let sceneChangeSequence = SKAction.sequence([waitToChangeScene, changeScene])
        self.run(sceneChangeSequence) //запускает изменение сцены
        }
}
