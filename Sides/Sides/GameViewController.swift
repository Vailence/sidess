//
//  GameViewController.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 06.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import UIKit
import GameplayKit
import SpriteKit
import AVFoundation
import GoogleMobileAds
import Firebase
import FirebaseDatabase
import FirebaseUI
//import FirebaseGoogleUI

var backMusic: AVAudioPlayer = AVAudioPlayer()

class GameViewController: UIViewController, GADBannerViewDelegate, FUIAuthDelegate {
    
    var ref: DatabaseReference!
    var fuiAuth: FUIAuth?
    var user: User?
    var handle: AuthStateDidChangeListenerHandle?
   
    
    @IBOutlet weak var BannerAD: GADBannerView!

    func setAuthCredentials(_ authUI: FUIAuth, didSignInWith user: User?){
        self.fuiAuth = authUI
        self.user = user!
        }
    
    override func viewDidAppear(_ animated: Bool) {
        }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        }
    
    override func viewDidLoad() {
        ref = Database.database().reference().child("users").childByAutoId()
        super.viewDidLoad()
        checkBanner()
        checkLoggedIn()
        FXstatus.isHidden = true
        let scene = MainMenuSScene (size: CGSize(width: 1536, height: 2048))
        if let view = self.view as! SKView? {
            scene.scaleMode = .aspectFill
            // Present the scene
            view.presentScene(scene)
            }
        do  {
            let audioPath = Bundle.main.path(forResource: "Patakas", ofType: "wav")
            try backMusic = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPath!) as URL)
            backMusic.numberOfLoops = -1
            }
        catch {
            }
        backMusic.play()
        }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                self.updHighScoreDate()
                }
            else{
                // No user is signed in.
                }
            }
        }
    
    override var shouldAutorotate: Bool {
        return true
        }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        }
        else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        }
 
    override var prefersStatusBarHidden: Bool {
        return true
        }
    
    func writeData() {
        let uid = Auth.auth().currentUser?.uid // !!!!
        let name = Auth.auth().currentUser?.displayName
        let post: [String: Any] = ["Name": name!,"Score": highScore, "UID": uid!]
        
        ref.setValue(post)
        }
    
    func updHighScoreDate() {
//        let updScore = Database.database().reference().child("users").childByAutoId()
//        updScore.updateChildValues(["Score": highScore])
        }
    
    func checkBanner() {
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID, "c8a6e9d6ebb2bb15960c18c2e51a23c9"]
        BannerAD.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        BannerAD.rootViewController = self
        BannerAD.delegate = self
        BannerAD.load(request)
    }
    
    func checkLoggedIn() {
        Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                 }
            else {
                self.login()
                }
            }
        }
    
    func login() {
        let authUI = FUIAuth.defaultAuthUI()
        authUI?.isSignInWithEmailHidden = true
        let googleProvider = FUIGoogleAuth()
        authUI?.delegate = self
        authUI?.providers = [googleProvider]
        let authViewController = authUI?.authViewController()
        self.present(authViewController!, animated: true, completion: nil)
//        Auth.auth().removeStateDidChangeListener(handle!)
       }
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        if error != nil {
            login()
             }
        else {
            //User is in! Here is where we code after signing in
            writeData()
            }
        }
}

