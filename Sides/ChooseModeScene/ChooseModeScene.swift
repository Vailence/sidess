//
//  ChooseModeScene.swift
//  Sides
//
//  Created by Akylbek Utekeshev on 09.02.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

let timerLabel = SKLabelNode(fontNamed: "CaviarDreams")
let survivalLabel = SKLabelNode(fontNamed: "CaviarDreams")

class ChooseModeScene: SKScene {

    override func didMove(to view: SKView) {
       
        // Задний фон
        let backgroundPic = SKSpriteNode(imageNamed: "preview")
        backgroundPic.size = self.size
        backgroundPic.position = CGPoint (x: self.size.width/2, y: self.size.height/2)
        backgroundPic.zPosition = -3
        self.addChild(backgroundPic)
        
        // Лого игры
        let logoImg = SKSpriteNode(imageNamed: "Logotype")
        logoImg.size = CGSize(width: 900, height: 900)
        logoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.74)
        logoImg.zPosition = -2
        self.addChild(logoImg)
        
        let textLogoImg = SKSpriteNode(imageNamed: "COLORS")
        textLogoImg.size = CGSize(width: 700, height: 150)
        textLogoImg.position = CGPoint(x: self.size.width/2, y: self.size.height*0.75)
        textLogoImg.zPosition = -1
        self.addChild(textLogoImg)
        
        let classicBtn = SKSpriteNode(imageNamed: "ClassicMode")
        classicBtn.size = CGSize(width: 300, height: 200)
        classicBtn.position = CGPoint(x: self.size.width/3, y: self.size.height*0.35)
        classicBtn.zPosition = 1
        classicBtn.name = "ClassicBtn"
        self.addChild(classicBtn)
        
        let survivalBtn = SKSpriteNode(imageNamed: "Survival")
        survivalBtn.size = CGSize(width: 300, height: 330)
        survivalBtn.position = CGPoint(x: self.size.width/1.5, y: self.size.height*0.35)
        survivalBtn.zPosition = 1
        survivalBtn.name = "SurvivalBtn"
        self.addChild(survivalBtn)

        let backLabel = SKSpriteNode(imageNamed: "Exit")
        backLabel.size = CGSize(width: 100, height: 100)
        backLabel.position = CGPoint(x: self.size.width/2, y: self.size.height*0.13)
        backLabel.zPosition = 1
        backLabel.name = "buttonBack"
        self.addChild(backLabel)
        
        let animateList = SKAction.sequence([SKAction.fadeIn(withDuration: 1.0), SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1.0)])
        let animateList2 = SKAction.sequence([SKAction.rotate(byAngle: -360/7, duration: 100)])
        
        textLogoImg.run(SKAction.repeatForever(animateList))
        logoImg.run(SKAction.repeatForever(animateList2))
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let pointITouches = touch.location (in: self)
            let tappedNode = atPoint(pointITouches)
            let tappedNodeName = tappedNode.name
            
            if tappedNodeName == "ClassicBtn"  {
                 livesNumber = 3
                let sceneToMoveTo = GameScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let sceneTransition = SKTransition.fade(withDuration: 0.5)
                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
            }
            
            if tappedNodeName == "SurvivalBtn" && highScore >= 10 {
                livesNumber = 1
                let sceneToMoveTo = GameScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let sceneTransition = SKTransition.fade(withDuration: 0.5)
                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
                
                let alert = UIAlertController(title: "Survival", message: "Your best score must be = 10", preferredStyle:UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay",
                                              style: UIAlertActionStyle.default,
                                              handler: {(alert: UIAlertAction!) in print("Okay")}))
                self.view?.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
                
            }
            
            if tappedNodeName == "SurvivalBtn" && highScore <= 10 {
                let alert = UIAlertController(title: "Survival", message: "Your best score must be = 10", preferredStyle:UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay",
                                              style: UIAlertActionStyle.default,
                                              handler: {(alert: UIAlertAction!) in print("Okay")}))
                self.view?.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
                
            }
            
            
            
            
//            if tappedNodeName == "timerLabel"  {
//                livesLabel.isHidden = true
//                let sceneToMoveTo = GameScene(size: self.size)
//                sceneToMoveTo.scaleMode = self.scaleMode
//                let sceneTransition = SKTransition.fade(withDuration: 0.5)
//                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
//            }
            if tappedNodeName == "buttonBack" {
                let sceneToMoveTo = MainMenuSScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let sceneTransition = SKTransition.fade(withDuration: 0.6)
                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
            }
        }
    }
}
